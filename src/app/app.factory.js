var appFactory = angular.module('DebtHackFactory', ['angular-underscore']);
var _apiServer = 'http://DebtHack.me/api'; //no trailing slash /

appFactory.factory('budgetFactory', function($q, $http, userFactory) {
    var factory = {};
    
    factory.getBudget = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/budget/' + userToken.id,
        });
    };
    
    factory.getBudgetRemaining = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/budget/remaining/' + userToken.id,
        });
    };
    
    factory.getBudgetDays = function () {
        return $http({
            method: 'GET',
            url: _apiServer + '/budget/days',
        });
    };
    
    factory.updateBudget = function(budget) {
        var userToken = userFactory.getUserToken();
        var postData = {
                user_id: userToken.id,
                budget_percent: budget.budget_percent,
                debt_percent: budget.debt_percent,
                weekly_income: budget.weekly_income,
                weekly_budget: budget.weekly_budget,
                towards_debt: budget.towards_debt,
                towards_savings: budget.towards_savings
            };
        return $http({
            method: 'POST',
            url: _apiServer + '/budget/create_or_update',
            data: postData
        });
    }
    
    return factory;
});

appFactory.factory('homeFactory', function($q, $http, budgetFactory, userFactory) {
    var factory = {};
    
    factory.getNotifications = function() {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/notifications/' + userToken.id,
        });
    };
    
    factory.dismissNotification = function(notification_id) {
        var postData = {
                notification_id: notification_id
            };
        return $http({
            method: 'POST',
            url: _apiServer + '/notifications/flag',
            data: postData
        });
    };
    
    return factory;
});

appFactory.factory('sharedFactory', function($q, $http, userFactory) {
    var factory = {};
    
    factory.getDebtTypes = function () {
        return $http({
            method: 'GET',
            url: _apiServer + '/types/debts',
        });
    };
    
    factory.getPayOffDate = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/budget/payoff_date/' + userToken.id,
        });
    };
    
    factory.getFrequencyTypes = function () {
        return $http({
            method: 'GET',
            url: _apiServer + '/types/frequencies',
        });
    };
    
    return factory; 
});

appFactory.factory('debtFactory', function(sharedFactory, userFactory, $q, $http) {
    var factory = {};
    
    factory.getDebts = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/debts/' + userToken.id,
        });
    };
    
    factory.pushDebt = function(newDebt) {
        if(typeof newDebt !== 'undefined')
        {
            var userToken = userFactory.getUserToken();
            var postData = {
                    user_id: userToken.id,
                    name: newDebt.name,
                    balance: newDebt.balance,
                    interestRate: newDebt.interestRate,
                    minPayment: newDebt.minPayment,
                    type: newDebt.selectedDebtType
                };
            return $http({
                method: 'POST',
                url: _apiServer + '/debts/create',
                data: postData
            });
        }
    };
    
    return factory;
});

appFactory.factory('billFactory', function(sharedFactory, userFactory, $q, $http) {
    var factory = {};
    
    factory.getBills = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/bills/' + userToken.id,
        });
    };
    
    factory.pushBill = function(newBill) {
        if(typeof newBill !== 'undefined')
        {
            var userToken = userFactory.getUserToken();
            var postData = {
                    user_id: userToken.id,
                    name: newBill.name,
                    amount: newBill.amount,
                    frequency: newBill.selectedFrequency
                };
            return $http({
                method: 'POST',
                url: _apiServer + '/bills/create',
                data: postData
            });
        }
    };
    
    return factory;
});

appFactory.factory('incomeFactory', function(sharedFactory, userFactory, $q, $http) {
    var factory = {};
    
    factory.getIncomeSources = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/income/' + userToken.id,
        });
    };
    
    factory.pushIncomeSource = function(newIncomeSource) {
        if(typeof newIncomeSource !== 'undefined')
        {
            var userToken = userFactory.getUserToken();
            var postData = {
                    user_id: userToken.id,
                    name: newIncomeSource.name,
                    amount: newIncomeSource.amount,
                    frequency: newIncomeSource.selectedFrequency
                };
            return $http({
                method: 'POST',
                url: _apiServer + '/income/create',
                data: postData
            });
        }
    };
    
    return factory;
});

appFactory.factory('userFactory', function($q, $http) {
    var userToken = null;
    var factory = {};
    var createdFlag = false;
    
    factory.doLogin = function(userName) {
        return $http({
            method: 'POST',
            url: _apiServer + '/users/create',
            data: {
                user_name: userName
            }
        });
    };
    
    factory.isLoggedIn = function() {
        if((typeof userToken == 'undefined') || userToken == null)
            return false;
        else 
            return true;
    }
    
    factory.isFirstLogin = function() {
        return createdFlag;
    }
    
    factory.setUserToken = function(token) {
        userToken = token;
    }
    
    factory.setCreatedFlag = function(flag) {
        createdFlag = flag;
    }
    
    factory.getUserToken = function() {
        return userToken;
    }
    
    factory.doLogout = function() {
        userToken = null;
    }
    
    return factory;
});

appFactory.factory('alternativeFactory', function($http, $q, sharedFactory, userFactory) {
    
    
    var factory = {};
    
    factory.getNewAlternatives = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/alternatives/' + userToken.id + '/new',
        });
    };
    
    factory.getAlternatives = function () {
        var userToken = userFactory.getUserToken();
        return $http({
            method: 'GET',
            url: _apiServer + '/alternatives/' + userToken.id,
        });
    };
    
    factory.pushIgnoreAlternative = function(alternative) {
        var userToken = userFactory.getUserToken();
        var postData = {
                user_id: userToken.id,
                alternative_id: alternative.alternatives[0].id,
                flag_id: alternative.id
            };
        return $http({
            method: 'POST',
            url: _apiServer + '/alternatives/ignore',
            data: postData
        });
    };
    
    factory.pushAlternative = function(alternative) {
        var userToken = userFactory.getUserToken();
        var postData = {
                user_id: userToken.id,
                alternative_id: alternative.selectedAltId,
                flag_id: alternative.id
            };
        return $http({
            method: 'POST',
            url: _apiServer + '/alternatives/assign',
            data: postData
        });
    };
    
    return factory;
});